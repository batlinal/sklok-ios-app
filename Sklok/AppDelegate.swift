//
//  AppDelegate.swift
//  Sklok
//
//  Created by Alex Batlin on 15/02/2015.
//  Copyright (c) 2015 Netdata Consultants. All rights reserved.
//

import UIKit
import CoreData
import AVFoundation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        let navigationController = self.window!.rootViewController as UINavigationController
        let controller = navigationController.topViewController as MasterViewController
        controller.managedObjectContext = self.managedObjectContext
        
        //create the snooze action
        let snoozeAction = UIMutableUserNotificationAction()
        snoozeAction.identifier = "SNOOZE_IDENTIFIER";
        snoozeAction.title = "Snooze";
        snoozeAction.activationMode = UIUserNotificationActivationMode.Background;
        snoozeAction.destructive = false;
        snoozeAction.authenticationRequired = false;
        
        // create the category
        let alarmCategory = UIMutableUserNotificationCategory()
        alarmCategory.identifier = "ALARM_CATEGORY"
        alarmCategory.setActions([snoozeAction], forContext: UIUserNotificationActionContext.Default)
        alarmCategory.setActions([snoozeAction], forContext: UIUserNotificationActionContext.Minimal)
        
        // create category set
        let categories = NSSet(array: [alarmCategory])
        
        // register for local notifications
        let types = UIUserNotificationType.Sound | UIUserNotificationType.Alert
        let settings = UIUserNotificationSettings(forTypes: types, categories: categories)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        
        return true
    }
    

    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {
        println("did register")
        println(application.currentUserNotificationSettings())
        println(notificationSettings)
        
        // FIXME: handle if user decided to turn off notifications e.g. warn user
    }
    
    func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {
        
        if let alarm = Alarm.alarmFromNotification(notification, persistentStoreCoordinator: persistentStoreCoordinator?, managedObjectContext: managedObjectContext?) {
            
            // de-activate expired non repeating alarm
            
            alarm.deactivateNonRepeatingAlarm()
            saveContext()
            
            // check if notification fired in foreground
            // in this case you need to show alert window as default is not to
            
            if application.applicationState == UIApplicationState.Active {
                
                // play the alert audio
                
                alarm.playSound()
                
                // show the alert dialog
                
                let alert = UIAlertController(title: "Sklock", message: alarm.label, preferredStyle: UIAlertControllerStyle.Alert)
                
                let defaultAction = UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default) {
                    action -> Void in
                    
                    alarm.stopSound()
                }
                
                alert.addAction(defaultAction)
                
                // add snooze button if enabled for alarm
                
                if alarm.snooze.integerValue > 0 {
                    
                    let snoozeAction = UIAlertAction(title: "Snooze", style: UIAlertActionStyle.Default) {
                        action -> Void in
                        
                        alarm.stopSound()
                        alarm.scheduleNotification(true)
                    }
                    
                    alert.addAction(snoozeAction)
                }
                
                let navigationController = self.window!.rootViewController as UINavigationController
                
                navigationController.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
    func application(application: UIApplication, handleActionWithIdentifier identifier: String?, forLocalNotification notification: UILocalNotification, completionHandler: () -> Void) {
        
        if let alarm = Alarm.alarmFromNotification(notification, persistentStoreCoordinator: persistentStoreCoordinator?, managedObjectContext: managedObjectContext?) {
        
            // de-activate expired non repeating alarm
            
            alarm.deactivateNonRepeatingAlarm()
            saveContext()
            
            if identifier == "SNOOZE_IDENTIFIER" {
                
                alarm.scheduleNotification(true)
            }
        }
        
        completionHandler();
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        let navigationController = self.window!.rootViewController as UINavigationController
        let controller = navigationController.topViewController as MasterViewController
        controller.cleanupNotifications()

    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "netdata.Sklok" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1] as NSURL
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("Sklok", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
        // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("Sklok.sqlite")
        var error: NSError? = nil
        var failureReason = "There was an error creating or loading the application's saved data."
        if coordinator!.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil, error: &error) == nil {
            coordinator = nil
            // Report any error we got.
            let dict = NSMutableDictionary()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason
            dict[NSUnderlyingErrorKey] = error
            error = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(error), \(error!.userInfo)")
            abort()
        }
        
        return coordinator
    }()

    lazy var managedObjectContext: NSManagedObjectContext? = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        if coordinator == nil {
            return nil
        }
        var managedObjectContext = NSManagedObjectContext()
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if let moc = self.managedObjectContext {
            var error: NSError? = nil
            if moc.hasChanges && !moc.save(&error) {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                NSLog("Unresolved error \(error), \(error!.userInfo)")
                abort()
            }
        }
    }
}

