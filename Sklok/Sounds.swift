//
//  Sounds.swift
//  Sklok
//
//  Created by Alex Batlin on 02/03/2015.
//  Copyright (c) 2015 Netdata Consultants. All rights reserved.
//

import Foundation

enum Sounds: String {
    case Digital = "Digital (Default):digital-alarm:wav"
    case Analog = "Analog:analog-alarm:wav"
    case Extreme = "Extreme:extreme-alarm:wav"
    
    static func soundsArray() -> Array<Sounds> {
        return [Sounds.Digital, Sounds.Analog, Sounds.Extreme]
    }
    
    static func labelAtIndex(index: Int) -> String {
        
        let labelComps = split(soundsArray()[index].rawValue) {$0 == ":"}
        return labelComps[0]
    }
    
    static func labelCompsAtIndex(index: Int) -> [String]? {
        
        return split(soundsArray()[index].rawValue) {$0 == ":"}
    }
    
    static func fileAtIndex(index: Int) -> NSURL? {
        
        if let labelComps = self.labelCompsAtIndex(index) {
            
            if let pathForSound = NSBundle.mainBundle().pathForResource(labelComps[1], ofType: labelComps[2]) {
                
                return NSURL(string: pathForSound)
            }
        }
       
        return nil
    }
    
    static func soundNameAtIndex(index: Int) -> String? {
        
        if let labelComps = self.labelCompsAtIndex(index) {
            
            return labelComps[1] + "." + labelComps[2]
        }
        
        return nil
    }
}