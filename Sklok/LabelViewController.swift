//
//  LabelViewController.swift
//  Sklok
//
//  Created by Alex Batlin on 21/02/2015.
//  Copyright (c) 2015 Netdata Consultants. All rights reserved.
//

import UIKit
import CoreData

class LabelViewController: UITableViewController {

    @IBOutlet weak var labelTextField: UITextField!
    
    var alarm: Alarm? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        
        if let alarm: Alarm = self.alarm {
            if alarm.label != self.labelTextField.text {

                alarm.label = self.labelTextField.text
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configureView() {
        // Update the user interface for the detail item.
        if let alarm: Alarm = self.alarm {
            if let label = self.labelTextField {
                label.text = alarm.label
            }
        }
    }
}
