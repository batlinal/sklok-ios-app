//
//  DetailViewController.swift
//  Sklok
//
//  Created by Alex Batlin on 15/02/2015.
//  Copyright (c) 2015 Netdata Consultants. All rights reserved.
//

import UIKit
import CoreData

class DetailViewController: UITableViewController {

    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var repeatCell: UITableViewCell!
    @IBOutlet weak var labelCell: UITableViewCell!
    @IBOutlet weak var soundCell: UITableViewCell!
    @IBOutlet weak var snoozeCell: UITableViewCell!

    var fetchedResultsController: NSFetchedResultsController? = nil

    var alarmItem: Alarm? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        self.configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    
    @IBAction func saveButtonPressed(sender: AnyObject) {
        self.saveAlarmChanges()
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    @IBAction func cancelButtonPressed(sender: AnyObject) {
        self.cancelAlarmChanges()
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    @IBAction func timeChanged(sender: UIDatePicker) {
        self.alarmItem?.time = self.datePicker.date
        self.alarmItem?.setDateToFuture()
    }
    
    @IBAction func deleteButtonPressed(sender: UIButton) {
        self.deleteAlarm()
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    // MARK: - Segues
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "showLabel" {
            if let alarm: Alarm = self.alarmItem {
                (segue.destinationViewController as LabelViewController).alarm = alarm
            }
        }
        
        if segue.identifier == "showRepeat" {
            if let alarm: Alarm = self.alarmItem {
                (segue.destinationViewController as RepeatViewController).alarm = alarm
            }
        }
        
        if segue.identifier == "showSound" {
            if let alarm: Alarm = self.alarmItem {
                (segue.destinationViewController as SoundViewController).alarm = alarm
            }
        }
        
        if segue.identifier == "showSnooze" {
            if let alarm: Alarm = self.alarmItem {
                (segue.destinationViewController as SnoozeViewController).alarm = alarm
            }
        }
    }
    
    // MARK: - Helpers
    
    func configureView() {
        // Update the user interface for the detail item.
        if let alarm: Alarm = self.alarmItem {
            
            if let label = self.labelCell {
                
                // label.text = alarm.valueForKey("timeStamp")!.description
                label.detailTextLabel?.text = alarm.label
            }
            
            if let snooze = self.snoozeCell {
                snooze.detailTextLabel?.text = SnoozeInterval.labelAtIndex(alarm.snooze.integerValue)
            }
            
            if let time = self.datePicker {
                time.date = alarm.time
            }
            
            if let repeat = self.repeatCell {
                repeat.detailTextLabel?.text = RepeatEvery.detailTextLabel(alarm.repeat.integerValue)
            }
            
            if let sound = self.soundCell {
                sound.detailTextLabel?.text = Sounds.labelAtIndex(alarm.sound.integerValue)
            }
        }
    }
    
    func saveAlarmChanges() {
        if let context = fetchedResultsController?.managedObjectContext {
            
            var error: NSError? = nil
            if context.hasChanges && !context.save(&error) {
                abort()
            }
        }
    }
    
    func cancelAlarmChanges() {
        
        if let alarm: Alarm = self.alarmItem {
            
            if let context = fetchedResultsController?.managedObjectContext {
            
                if alarm.objectID.temporaryID == false {
                
                    context.refreshObject(alarm, mergeChanges: false)
                }
                else {
                    
                    context.deleteObject(alarm)
                }
            }
        }
    }
    
    func deleteAlarm() {
        if let alarm: Alarm = self.alarmItem {
            
            if let context = fetchedResultsController?.managedObjectContext {
                
                context.deleteObject(alarm)
                
                var error: NSError? = nil
                
                if !context.save(&error) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    //println("Unresolved error \(error), \(error.userInfo)")
                    abort()
                }
            }
        }
    }
    
}

