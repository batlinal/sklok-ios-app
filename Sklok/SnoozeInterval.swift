//
//  SnoozeInterval.swift
//  Sklok
//
//  Created by Alex Batlin on 17/03/2015.
//  Copyright (c) 2015 Netdata Consultants. All rights reserved.
//

import Foundation

enum SnoozeInterval: String {
    case NoSnooze = "No Snooze"
    case OneMinute = "1 Minute"
    case ThreeMinutes = "3 Minutes"
    case FiveMinutes = "5 Minutes"
    case TenMinutes = "10 Minutes"
    case FifteenMinutes = "15 Minutes"

    static func snoozeArray() -> Array<SnoozeInterval> {
        return [self.NoSnooze, self.OneMinute, self.ThreeMinutes, self.FiveMinutes, self.TenMinutes, self.FifteenMinutes]
    }
    
    static func labelAtIndex(index: Int) -> String {
        return snoozeArray()[index].rawValue
    }
    
}