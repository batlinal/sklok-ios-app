//
//  SoundViewController.swift
//  Sklok
//
//  Created by Alex Batlin on 21/02/2015.
//  Copyright (c) 2015 Netdata Consultants. All rights reserved.
//

import UIKit
import CoreData
import AVFoundation

class SoundViewController: UITableViewController {
    
    var alarm: Alarm?
    var audioPlayer: AVAudioPlayer?

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        if var alarm: Alarm = self.alarm {
            
            self.tableView.beginUpdates()
            
            alarm.sound = indexPath.row
            
            for index in 0 ..< Sounds.soundsArray().count {
                let cellIndex = NSIndexPath(forRow: index, inSection: 0)!
                self.configureCell(tableView.cellForRowAtIndexPath(cellIndex)!, atIndexPath: cellIndex)
            }
            
            self.tableView.endUpdates()
            
            alarm.playSound()
        }
    }
    
    func configureCell(cell: UITableViewCell, atIndexPath indexPath: NSIndexPath) {
        
        cell.textLabel?.text = Sounds.labelAtIndex(indexPath.row)
        
        if let alarm: Alarm = self.alarm {
            let currentSound = alarm.sound
            
            if currentSound == indexPath.row {
                cell.accessoryType = UITableViewCellAccessoryType.Checkmark
            }
            else {
                cell.accessoryType = UITableViewCellAccessoryType.None
            }
        }
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Sounds.soundsArray().count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("soundCell", forIndexPath: indexPath) as UITableViewCell
        self.configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    override func viewWillDisappear(animated: Bool) {
        
        if var alarm: Alarm = self.alarm {
            
            alarm.stopSound()
        }
    }

}
