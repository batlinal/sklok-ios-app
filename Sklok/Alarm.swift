//
//  Alarm.swift
//  Sklok
//
//  Created by Alex Batlin on 25/02/2015.
//  Copyright (c) 2015 Netdata Consultants. All rights reserved.
//

import Foundation
import CoreData
import UIKit
import AVFoundation

@objc(Alarm)
class Alarm: NSManagedObject {
    
    var audioPlayer: AVAudioPlayer?

    @NSManaged var active: NSNumber
    @NSManaged var label: String
    @NSManaged var repeat: NSNumber
    @NSManaged var snooze: NSNumber
    @NSManaged var sound: NSNumber
    @NSManaged var time: NSDate
    
    class func alarmFromNotification(notification: UILocalNotification, persistentStoreCoordinator: NSPersistentStoreCoordinator?, managedObjectContext: NSManagedObjectContext?) -> Alarm? {
        
        if let userInfo = notification.userInfo {
            
            if let alarmUriString = userInfo["alarmUri"] as? String {
                
                if let alarmUri = NSURL(string: alarmUriString) {
                    
                    if let alarmObjectId = persistentStoreCoordinator?.managedObjectIDForURIRepresentation(alarmUri) {
                        
                        var error: NSError? = nil
                        
                        if let alarm = managedObjectContext?.existingObjectWithID(alarmObjectId, error: &error) as? Alarm {
                            
                            return alarm
                            
                        }
                        
                    }
                }
            }
        }
        
        return nil
    }
    
    func notification() -> UILocalNotification? {
        
        let alarmUri = self.objectID.URIRepresentation().description
        
        if self.objectID.temporaryID == false {
            
            for notification in UIApplication.sharedApplication().scheduledLocalNotifications as [UILocalNotification] {
                
                if let userInfo = notification.userInfo? {
                    
                    let thisAlarmUri:String = userInfo["alarmUri"] as String
                    
                    if thisAlarmUri == alarmUri {
                        
                        println("alarm found")
                        
                        return notification
                    }
                }
            }
        }
        
        println("alarm not found")
        
        return nil
    }
    
    // Should be called when the active flag is flipped
    // makes sure that the date part of alarm time is set to either today, if alarm time is in the future, or next day
    
    func setDateToFuture() {
        
        let calendar = NSCalendar.autoupdatingCurrentCalendar()
        let currentDate = NSDate()
        
        let currentDateComps = calendar.componentsInTimeZone(NSTimeZone.defaultTimeZone(), fromDate: currentDate)
        let alarmDateComps = calendar.componentsInTimeZone(NSTimeZone.defaultTimeZone(), fromDate: self.time)
        
        currentDateComps.hour = alarmDateComps.hour
        currentDateComps.minute = alarmDateComps.minute
        currentDateComps.second = 0
        
        if let newTime = calendar.dateFromComponents(currentDateComps) {
            
            if (newTime.compare(currentDate) == NSComparisonResult.OrderedAscending) {
                
                self.time = newTime.dateByAddingTimeInterval(60*60*24)
            }
            else {
                
                self.time = newTime
            }
            
            println("Set alarm time to  " + self.time.description)
        }
    }
    
    func scheduleNotification(snooze: Bool) {
        
        let notification = UILocalNotification()

        notification.timeZone = NSTimeZone.defaultTimeZone()
        notification.alertBody = self.label
        notification.alertTitle = "Alarm"
        
        // if snooze button was pressed instead of schedulling it first time round
        if snooze == true {
            
            // snooze for configured ammount of time
            notification.fireDate = NSDate().dateByAddingTimeInterval(60 * self.snooze.doubleValue)
        }
        else {
            
            notification.fireDate = self.time
        }
        
        if let alertSound = Sounds.soundNameAtIndex(self.sound.integerValue) {

            notification.soundName = alertSound
        }
        else {
            
            notification.soundName = UILocalNotificationDefaultSoundName
        }
        
        if self.snooze.integerValue > 0 {
            
            notification.category = "ALARM_CATEGORY"
        }

        notification.alertAction = "Dismiss"
        
        notification.userInfo = ["alarmUri": self.objectID.URIRepresentation().description]
        println(self.objectID.URIRepresentation().description)
        
        UIApplication.sharedApplication().scheduleLocalNotification(notification)
        
        println("notification schedulled")
    }
    
    func cancelNotification() {
        
        println("notification being cancelled")
        
        if let notification = self.notification() {
            UIApplication.sharedApplication().cancelLocalNotification(notification)
            println("notification cancelled")
        }
    }
    
    // should be called either from app delegate in response to notification or when app is activate and alarms are cleaned up
    // deactivate if non-repeating, active and set for a past date
    
    func deactivateNonRepeatingAlarm() {
        
        if (self.repeat == 0) & (self.active == true) & (self.time.compare(NSDate()) == NSComparisonResult.OrderedAscending) {

            self.active = false
        }
    }
    
    func playSound() {
        if let alertSound = Sounds.fileAtIndex(self.sound.integerValue) {
            
            self.stopSound()
            
            audioPlayer = AVAudioPlayer(contentsOfURL: alertSound, error: nil)
            audioPlayer!.prepareToPlay()
            audioPlayer!.play()
        }
    }
    
    func stopSound() {
        
        if let player = audioPlayer {
            
            if player.playing == true {
                
                player.stop()
            }
        }
    }
    
    override func validateForUpdate(error: NSErrorPointer) -> Bool {
        let propertiesValid = super.validateForUpdate(error)
        println("validating on update")
        
        if self.active == true {
            
            self.cancelNotification()
            self.scheduleNotification(false)
        }
        else {
            self.cancelNotification()
        }
        
        //        BOOL consistencyValid = [self validateConsistency:error];
        //        return (propertiesValid && consistencyValid);
        
        return propertiesValid
    }
    
    override func validateForInsert(error: NSErrorPointer) -> Bool {
        let propertiesValid = super.validateForUpdate(error)
        println("validating on insert")
        
        self.scheduleNotification(false)
        
        return propertiesValid
    }
    
    
    override func validateForDelete(error: NSErrorPointer) -> Bool {
        let propertiesValid = super.validateForUpdate(error)
        println("validating on delete")
        
        self.cancelNotification()
        
        return propertiesValid
    }

}
