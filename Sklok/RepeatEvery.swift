//
//  RepeatEvery.swift
//  Sklok
//
//  Created by Alex Batlin on 02/03/2015.
//  Copyright (c) 2015 Netdata Consultants. All rights reserved.
//

import Foundation

enum RepeatEvery: String {
    case Mon = "Every Monday"
    case Tue = "Every Tuesday"
    case Wed = "Every Wednesday"
    case Thu = "Every Thursday"
    case Fri = "Every Friday"
    case Sat = "Every Saturday"
    case Sun = "Every Sunday"
    
    static func dayArray() -> Array<RepeatEvery> {
        return [RepeatEvery.Mon, RepeatEvery.Tue, RepeatEvery.Wed, RepeatEvery.Thu, RepeatEvery.Fri, RepeatEvery.Sat, RepeatEvery.Sun]
    }
    
    static func labelAtIndex(index: Int) -> String {
        return dayArray()[index].rawValue
    }
    
    static func detailTextLabel(alarmValue: Int) -> String {
        switch (alarmValue) {
        case 0:
            return "No days selected"
        case 96:
            return "Weekends"
        case 31:
            return "Weekdays"
        default:
            return "Mixed days"
        }
    }
}