//
//  AlarmCell.swift
//  Sklok
//
//  Created by Alex Batlin on 16/02/2015.
//  Copyright (c) 2015 Netdata Consultants. All rights reserved.
//

import UIKit
import CoreData

class AlarmCell: UITableViewCell {
    
    var alarmSwitch = UISwitch()
    
    var fetchedResultsController: NSFetchedResultsController? = nil
    
    var alarm: Alarm? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }
    
    func configureView() {
        // Update the user interface for the detail item.
        if let alarm: Alarm = self.alarm {
            
            alarmSwitch.on = alarm.active.boolValue
            self.accessoryView = alarmSwitch;
            alarmSwitch.addTarget(self, action: Selector("switchValueChanged:"), forControlEvents: .ValueChanged)
            
            var formatter = NSDateFormatter()
            formatter.dateFormat = "HH:mm"
            self.textLabel?.text = formatter.stringFromDate(alarm.time)
            
            self.detailTextLabel?.text = alarm.label
            
            if self.alarmSwitch.on == true {
                self.backgroundColor = UIColor.whiteColor()
            }
            else {
                self.backgroundColor = UIColor.groupTableViewBackgroundColor()
            }
        }
    }

    @IBAction func switchValueChanged(sender: UISwitch) {
        
        if let alarm: Alarm = self.alarm {
            
            if let context = fetchedResultsController?.managedObjectContext {

                alarm.active = self.alarmSwitch.on
                alarm.setDateToFuture()
                
                var error: NSError? = nil
                if context.hasChanges && !context.save(&error) {
                    abort()
                }
            }
        }
    }

}
