//
//  SnoozeViewController.swift
//  Sklok
//
//  Created by Alex Batlin on 17/03/2015.
//  Copyright (c) 2015 Netdata Consultants. All rights reserved.
//

import UIKit

class SnoozeViewController: UITableViewController {

    var alarm: Alarm?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if var alarm: Alarm = self.alarm {
            
            self.tableView.beginUpdates()
            
            alarm.snooze = indexPath.row
            
            for index in 0 ..< SnoozeInterval.snoozeArray().count {
                let cellIndex = NSIndexPath(forRow: index, inSection: 0)!
                self.configureCell(tableView.cellForRowAtIndexPath(cellIndex)!, atIndexPath: cellIndex)
            }
            
            self.tableView.endUpdates()
        }
    }
    
    func configureCell(cell: UITableViewCell, atIndexPath indexPath: NSIndexPath) {
        
        cell.textLabel?.text = SnoozeInterval.labelAtIndex(indexPath.row)
        
        if let alarm: Alarm = self.alarm {
            
            let currentSnooze = alarm.snooze
            
            if currentSnooze == indexPath.row {
                cell.accessoryType = UITableViewCellAccessoryType.Checkmark
            }
            else {
                cell.accessoryType = UITableViewCellAccessoryType.None
            }
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SnoozeInterval.snoozeArray().count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("snoozeCell", forIndexPath: indexPath) as UITableViewCell
        self.configureCell(cell, atIndexPath: indexPath)
        return cell
    }
}
