//
//  RepeatViewController.swift
//  Sklok
//
//  Created by Alex Batlin on 21/02/2015.
//  Copyright (c) 2015 Netdata Consultants. All rights reserved.
//

import UIKit
import CoreData

class RepeatViewController: UITableViewController {
    
    var alarm: Alarm?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source
    
    func configureCell(cell: UITableViewCell, atIndexPath indexPath: NSIndexPath) {
        cell.textLabel?.text = RepeatEvery.labelAtIndex(indexPath.row)
        
        if let alarm: Alarm = self.alarm {
            let currentAlarm = alarm.repeat.unsignedCharValue
            let umask = self.umask(indexPath)
            
            if (currentAlarm & umask) == umask {
                cell.accessoryType = UITableViewCellAccessoryType.Checkmark
            }
            else {
                cell.accessoryType = UITableViewCellAccessoryType.None
            }
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
            
        if var alarm: Alarm = self.alarm {
            
            self.tableView.beginUpdates()
            
            let currentAlarm = alarm.repeat.unsignedCharValue
            let umask = self.umask(indexPath)
            
            if (currentAlarm & umask) == umask {
                alarm.repeat = NSNumber(unsignedChar: currentAlarm - umask)
            }
            else {
                alarm.repeat = NSNumber(unsignedChar: currentAlarm + umask)
            }
            
            self.configureCell(tableView.cellForRowAtIndexPath(indexPath)!, atIndexPath: indexPath)
            
            self.tableView.endUpdates()
        }
    }
    
    func umask(indexPath: NSIndexPath) -> UInt8 {
        return UInt8(pow(Double(2), Double(indexPath.row)))
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return RepeatEvery.dayArray().count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("dayCell", forIndexPath: indexPath) as UITableViewCell
        self.configureCell(cell, atIndexPath: indexPath)
        return cell
    }
}
